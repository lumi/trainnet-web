{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  
  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs { inherit system; };
    in {
      devShells.default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          nodejs
          yarn
          pkgs.nodePackages.typescript-language-server
        ];
      };
    }) // {
      pognix.extraModules = [({ ... }: {
        pognix.network.mappings = [
          "5455:5455"
        ];
      })];
    };
}
