/// <reference types="vite/client" />

import './style.css'

import { render } from 'preact';

import { Connection, ServerFrame } from "./connection";
import { Viewport, v2 } from './math';
import { WorldView } from './view';
import { World } from './world';
import { handleFrame } from './frame';
import { useEffect, useRef, useState } from 'preact/hooks';
import { debouncer } from './debounce';
import { REQUEST_SCALE } from './const';

const conn = new Connection("ws://localhost:5454");

const world = new World();

world.run();

const startViewport = new Viewport(
  v2(
    parseFloat(import.meta.env.VITE_START_X),
    parseFloat(import.meta.env.VITE_START_Y),
  ),
  parseFloat(import.meta.env.VITE_START_ZOOM),
);

const App = () => {
  const debouncerRef = useRef(debouncer(200, true));

  const [viewport, setViewport] = useState<Viewport>(startViewport);
  const [size, setSize] = useState(v2(window.innerWidth, window.innerHeight));

  useEffect(() => {
    const onFrame = (frame: ServerFrame) => {
      handleFrame(world, frame);
    };

    conn.on("frame", onFrame);

    return () => {
      conn.off("frame", onFrame);
    };
  }, []);

  useEffect(() => {
    debouncerRef.current(() => {
      const vp = viewport.toAabb(size);
      conn.setViewport(vp.scale(REQUEST_SCALE));
    });
  }, [viewport]);

  useEffect(() => {
    const onResize = (_evt: Event) => {
      setSize(v2(window.innerWidth, window.innerHeight));
    };

    window.addEventListener("resize", onResize);

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  return <WorldView
    world={world}
    viewport={viewport}
    size={size}
    onViewportChange={vp => setViewport(vp)}
    drawShapes
    drawLinks
  />;
};

render(<App />, document.body);
