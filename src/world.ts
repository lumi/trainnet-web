import { DESIRED_LAG, MAX_LAG, TICK_MS, LAG_TOLERANCE, MAX_TRAIN_STATES } from "./const";
import { Vec2 } from "./math";

export type Shape = {
  id: number,
  type: number;
  points: Vec2[];
};

export type RailLink = {
  end: number,
  blockId: number,
};

export type RailNode = {
  id: number,
  pos: Vec2,
  next: RailLink[],
};

export type WagonState = {
  pos: Vec2,
  dir: Vec2,
  length: number,
};

export type TrainState = {
  time: number,
  pos: Vec2,
  dir: Vec2,
  speed: number,
  pathLength: number,
  wagons: WagonState[],
};

export type Train = {
  id: number,
  states: TrainState[],
};

export class World {
  time: number;
  timeAt: number;
  rate: number;
  serverTime: number;
  trains: Map<number, Train>;
  nodes: Map<number, RailNode>;
  shapes: Shape[];

  #timeout: number | undefined;

  constructor() {
    this.time = 0;
    this.timeAt = Date.now();
    this.rate = 0;
    this.serverTime = 0;
    this.trains = new Map();
    this.nodes = new Map();
    this.shapes = [];

    this.#timeout = undefined;
  }

  run(): void {
    if (this.#timeout) {
      return;
    }

    this.step();

    this.#timeout = setTimeout(() => {
      this.#timeout = undefined;
      this.run()
    }, TICK_MS);
  }

  stop(): void {
    if (this.#timeout) {
      clearTimeout(this.#timeout);
    }
  }

  step(): void {
    let delta = this.serverTime - this.time;

    if (delta > MAX_LAG) {
      this.time = this.serverTime - MAX_LAG;
      delta = MAX_LAG;
    } else if (delta < LAG_TOLERANCE) {
      this.time = this.serverTime - LAG_TOLERANCE;
      delta = LAG_TOLERANCE;
    }

    const norm = delta / DESIRED_LAG;

    const tickSeconds = TICK_MS / 1000;

    this.rate = norm * norm * norm;
    this.time += this.rate * tickSeconds;
    this.timeAt = Date.now();

    for (const train of this.trains.values()) {
      train.states = train.states.filter(s => s.time >= this.time - tickSeconds * 2);

      if (train.states.length > MAX_TRAIN_STATES) {
        console.warn("train states very large", train.states.length);
        const stateSet = new Set();
        for (const state of train.states) {
          if (stateSet.has(state.time)) {
            console.warn("duplicate time", state.time);
            break;
          }
          stateSet.add(state.time);
        }
        debugger;
        train.states = train.states.slice(0, MAX_TRAIN_STATES);
      }

      if (train.states.length === 0) {
        this.trains.delete(train.id);
      }
    }
  }

  addNode(id: number, pos: Vec2): void {
    this.nodes.set(id, {
      id,
      pos,
      next: []
    });
  }

  delNode(id: number): void {
    this.nodes.delete(id);
  }

  addLink(start: number, end: number, blockId: number): void {
    const node = this.nodes.get(start);

    if (node) {
      node.next.push({ end, blockId });
    }
  }

  delLink(start: number, end: number): void {
    const node = this.nodes.get(start);

    if (node) {
      node.next = node.next.filter(l => l.end !== end);
    }
  }

  addShape(id: number, type: number, points: Vec2[]): void {
    this.shapes.push({
      id,
      type,
      points,
    });
  }

  delShape(id: number): void {
    const idx = this.shapes.findIndex(s => s.id === id);

    if (idx >= 0) {
      this.shapes.splice(idx, 1);
    }
  }

  updateTime(time: number): void {
    this.serverTime = time;

    if (this.time === 0) {
      this.time = time - DESIRED_LAG;
    }
  }

  updateTrain(id: number, time: number, pos: Vec2, dir: Vec2, speed: number, pathLength: number, wagons: { length: number, pos: Vec2, dir: Vec2 }[]): void {
    let train = this.trains.get(id);

    if (!train) {
      train = {
        id,
        states: [],
      };

      this.trains.set(train.id, train);
    }

    train.states.push({
      time,
      pos,
      dir,
      speed,
      pathLength,
      wagons,
    });
  }
}
