import { Vec2, v2 } from "./math";

export class DrawContext {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;

  constructor(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D) {
    this.canvas = canvas;
    this.ctx = ctx;
  }

  static fromCanvas(canvas: HTMLCanvasElement) {
    const ctx = canvas.getContext("2d");

    if (!ctx) {
      throw new Error("cannot get 2d context");
    }

    return new DrawContext(canvas, ctx);
  }

  position(): Vec2 {
    return v2(this.canvas.offsetLeft, this.canvas.offsetTop);
  }

  size(): Vec2 {
    return v2(this.canvas.width, this.canvas.height);
  }

  clearAll(): void {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  arc(pos: Vec2, radius: number, start: number = 0, end: number = 2 * Math.PI): void {
    this.ctx.arc(pos.x, pos.y, radius, start, end);
  }

  save(cb: () => void): void {
    try {
      this.ctx.save();
      cb();
    }
    finally {
      this.ctx.restore();
    }
  }

  translate(offset: Vec2): void {
    this.ctx.translate(offset.x, offset.y);
  }

  rotate(theta: number): void {
    this.ctx.rotate(theta);
  }

  scale(xf: number, yf: number): void {
    this.ctx.scale(xf, yf);
  }

  openPath(cb: () => void): void {
    this.ctx.beginPath();
    cb();
  }

  closedPath(cb: () => void): void {
    this.ctx.beginPath();
    cb();
    this.ctx.closePath();
  }

  rect(x: number, y: number, w: number, h: number): void {
    this.ctx.rect(x, y, w, h);
  }

  setLineWidth(width: number): void {
    this.ctx.lineWidth = width;
  }

  setFillStyle(style: string): void {
    this.ctx.fillStyle = style;
  }

  setStrokeStyle(style: string): void {
    this.ctx.strokeStyle = style;
  }

  fill(): void {
    this.ctx.fill();
  }

  stroke(): void {
    this.ctx.stroke();
  }

  fillText(pos: Vec2, text: string): void {
    this.ctx.fillText(text, pos.x, pos.y);
  }

  moveTo(pos: Vec2): void {
    this.ctx.moveTo(pos.x, pos.y);
  }

  lineTo(pos: Vec2): void {
    this.ctx.lineTo(pos.x, pos.y);
  }
}
