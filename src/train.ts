import { Vec2, angleInterpolate } from "./math";
import { TrainState, World } from "./world";

export function getNow(world: World): number {
  return world.time + (Date.now() - world.timeAt) * world.rate / 1000;
}

export function getCurrentTrainState(now: number, states: TrainState[]): { pos: Vec2, rot: number, pathLength: number, wagons: { pos: Vec2, rot: number, length: number }[] } | undefined {
  let sa: TrainState | undefined, sb: TrainState | undefined;

  for (let i = 0; i < states.length - 1; i++) {
    const j = i + 1;
    if (states[i].time <= now && states[j].time >= now) {
      sa = states[i];
      sb = states[j];
    }
  }

  if (!sa || !sb) {
    return;
  }

  const t = (now - sa.time) / (sb.time - sa.time);

  const pos = Vec2.lerp(t, sa.pos, sb.pos);

  const wagons = [];

  const wagonCount = Math.min(sa.wagons.length, sb.wagons.length);

  for (let i = 0; i < wagonCount; i++) {
    const wa = sa.wagons[i];
    const wb = sb.wagons[i];
    wagons.push({
      pos: Vec2.lerp(t, wa.pos, wb.pos),
      rot: angleInterpolate(t, wa.dir.angle(), wb.dir.angle()),
      length: wb.length,
    });
  }

  return {
    pos,
    rot: angleInterpolate(t, sa.dir.angle(), sb.dir.angle()),
    pathLength: sb.pathLength,
    wagons
  };
}
