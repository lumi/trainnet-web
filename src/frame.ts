import { Vec2 } from "./math";
import { ServerFrame } from "./connection";
import { World } from "./world";

export function handleFrame(world: World, frame: ServerFrame): void {
  if (frame.type === "dynamic") {
    world.updateTime(frame.time);
    for (const train of frame.trains) {
      world.updateTrain(
        train.id,
        frame.time,
        Vec2.fromArray(train.pos),
        Vec2.fromArray(train.dir),
        train.speed,
        train.path_length,
        train.wagons.map(w => ({ pos: Vec2.fromArray(w.pos), dir: Vec2.fromArray(w.dir), length: w.length })),
      );
    }
  } else if (frame.type === "bulk") {
    for (const id of frame.del_nodes) {
      world.delNode(id);
    }
    for (const [start, end] of frame.del_links) {
      world.delLink(start, end);
    }
    for (const id of frame.del_shapes) {
      world.delShape(id);
    }
    for (const [id, pos] of frame.add_nodes) {
      world.addNode(id, Vec2.fromArray(pos));
    }
    for (const [[start, end], block_id] of frame.add_links) {
      world.addLink(start, end, block_id);
    }
    for (const [id, type, points] of frame.add_shapes) {
      world.addShape(id, type, points.map(Vec2.fromArray));
    }
  }
};
