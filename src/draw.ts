import { SHAPE_COLORS } from "./const";
import { DrawContext } from "./draw_context";
import { Vec2, v2, Aabb2, Viewport } from "./math";
import { formatTimestamp } from "./time";
import { getCurrentTrainState, getNow } from "./train";
import { World } from "./world";

const WAGON_WIDTH = 4;

type DrawOpts = {
  drawShapes?: boolean;
  drawNodes?: boolean;
  drawLinks?: boolean;
  debug?: boolean;
};

function drawTrains(world: World, ctx: DrawContext): void {
  const now = getNow(world);
  for (const train of world.trains.values()) {
    const state = getCurrentTrainState(now, train.states);
    if (!state) {
      continue;
    }
    ctx.save(() => {
      ctx.translate(state.pos);
      ctx.rotate(state.rot);
      ctx.closedPath(() => {
        ctx.moveTo(v2(4, 0));
        ctx.lineTo(v2(0, -WAGON_WIDTH * 0.5));
        ctx.lineTo(v2(0, WAGON_WIDTH * 0.5));
      });
      ctx.setFillStyle("#f00");
      ctx.fill();
    });
    for (const wagon of state.wagons) {
      ctx.save(() => {
        ctx.translate(wagon.pos);
        ctx.rotate(wagon.rot);
        ctx.closedPath(() => {
          ctx.rect(-wagon.length * 0.5, -WAGON_WIDTH * 0.5, wagon.length, WAGON_WIDTH);
        });
        ctx.setFillStyle("#00f");
        ctx.fill();
      });
    }
    ctx.save(() => {
      ctx.translate(state.pos);
      ctx.scale(1.5, 1.5);
      ctx.fillText(v2(0, 10), `${train.id} (${state.pathLength})`);
    });
  }
}

function drawMap(world: World, ctx: DrawContext, vp: Aabb2, opts: DrawOpts = {}): void {
  if (opts.drawShapes) {
    ctx.save(() => {
      for (const shape of world.shapes) {
        const { points } = shape;
        ctx.closedPath(() => {
          for (let i = 0; i < points.length; i++) {
            if (i === 0) {
              ctx.moveTo(points[i]);
            }
            else {
              ctx.lineTo(points[i]);
            }
          }
        });
        const color = SHAPE_COLORS[shape.type];
        ctx.setFillStyle(color);
        ctx.fill();
      }
    });
  }
  if (opts.drawLinks) {
    ctx.save(() => {
      ctx.setLineWidth(1);
      ctx.setStrokeStyle("#999");
      for (const node of world.nodes.values()) {
        const nodeInVp = vp.containsPoint(node.pos);
        for (const edge of node.next) {
          const next = world.nodes.get(edge.end);
          if (!next) {
            continue;
          }
          const nextInVp = vp.containsPoint(next.pos);
          if (!nodeInVp && !nextInVp) {
            continue;
          }
          ctx.save(() => {
            ctx.openPath(() => {
              ctx.moveTo(node.pos);
              ctx.lineTo(next.pos);
            });
            const hue = (edge.blockId * 7757) % 360;
            ctx.setStrokeStyle(`hsl(${Math.floor(hue)}deg, 50%, 50%)`);
            ctx.stroke();
            if (opts.debug) {
              ctx.translate(node.pos.scale(0.7).add(next.pos.scale(0.3)).add(v2(2, 3)));
              ctx.scale(0.7, 0.7);
              ctx.fillText(v2(0, 0), `${edge.blockId}`);
            }
          });
        }
      }
    });
  }
  if (opts.drawNodes) {
    for (const node of world.nodes.values()) {
      if (!vp.containsPoint(node.pos)) {
        continue;
      }
      let color: string;
      let radius: number;
      switch (node.next.length) {
        case 0:
          color = "#f00";
          radius = 1;
          break;
        case 1:
          color = "#f00";
          radius = 1;
          break;
        case 2:
          color = "#000";
          radius = 1;
          break;
        default:
          color = "#0f0";
          radius = node.next.length - 1;
      }
      ctx.save(() => {
        ctx.setFillStyle(color);
        ctx.closedPath(() => {
          ctx.arc(node.pos, radius, 0, 2 * Math.PI);
        });
        ctx.fill();
        if (opts.debug) {
          ctx.translate(node.pos);
          ctx.fillText(v2(2, -2), `${node.id}`);
        }
      });
    }
  }
}

export function draw(world: World, ctx: DrawContext, viewport: Viewport, opts: DrawOpts = {}): void {
  ctx.clearAll();
  const scale = viewport.scale();
  const vp = viewport.toAabb(ctx.size());
  const rq = vp.scale(1.1);
  ctx.save(() => {
    ctx.translate(viewport.center.scale(-scale).add(ctx.size().scale(0.5)));
    ctx.scale(scale, scale);
    drawMap(world, ctx, rq, opts);
    drawTrains(world, ctx);
  });
  ctx.save(() => {
    const scaleChar = scale > 1 ? "*" : "/";
    const scaleFactor = scale > 1 ? Math.floor(scale * 100) / 100 : Math.floor((1 / scale) * 100) / 100;

    const now = getNow(world);

    const trainStates: [number, number, { pos: Vec2, rot: number }][] = [];

    for (const train of world.trains.values()) {
      const state = getCurrentTrainState(now, train.states);

      if (state) {
        const dist = Vec2.distance(state.pos, viewport.center);
        trainStates.push([train.id, dist, state]);
      }
    }

    trainStates.sort((a, b) => a[1] - b[1]);

    let nearestNode = undefined;
    let nearestNodeDist = Infinity;

    for (const node of world.nodes.values()) {
      const dist = Vec2.distance(node.pos, viewport.center);

      if (dist < nearestNodeDist) {
        nearestNode = node;
        nearestNodeDist = dist;
      }
    }

    let offset = 10;

    const addLine = (line: string): void => {
      ctx.fillText(v2(0, offset), line);
      offset += 10;
    };

    ctx.translate(v2(5, 5));

    addLine(`client time ${formatTimestamp(world.time)}`);
    addLine(`server time ${formatTimestamp(world.serverTime)}`);
    addLine(`node count ${world.nodes.size}`);
    addLine(`center ${viewport.center} zoom ${Math.floor(viewport.zoom * 100) / 100} (scale ${scaleChar} ${scaleFactor})`);
    addLine(`client vp (${Math.floor(vp.min_x)}, ${Math.floor(vp.min_y)}) to (${Math.floor(vp.max_x)}, ${Math.floor(vp.max_y)})`);
    addLine(`culling vp (${Math.floor(rq.min_x)}, ${Math.floor(rq.min_y)}) to (${Math.floor(rq.max_x)}, ${Math.floor(rq.max_y)})`);

    if (nearestNode) {
      addLine(`nearest node ${nearestNode.id} at ${nearestNode.pos}`);
    }

    offset += 5;

    addLine("trains:");
    for (const [id, dist, state] of trainStates.slice(0, 16)) {
      addLine(`${id} at ${state.pos} rot ${Math.floor(state.rot / Math.PI * 180)} dist ${Math.floor(dist)}`);
    }
  });
}

