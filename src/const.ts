export const DESIRED_LAG = 2;
export const LAG_TOLERANCE = 1;
export const MAX_LAG = DESIRED_LAG * 2;
export const MAX_TRAIN_STATES = 100;
export const REQUEST_SCALE = 3;
export const SHAPE_COLORS = ["#aaa", "#00f"]
export const TICK_MS = 100;
