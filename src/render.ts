export type RenderCallback = () => void;

export class RenderLoop {
  #callback: RenderCallback;
  #rafHandle: number | undefined;

  constructor(callback: RenderCallback) {
    this.#callback = callback;
    this.#rafHandle = undefined;
  }

  run() {
    if (this.#rafHandle) {
      return;
    }
    this.#callback();
    this.#rafHandle = requestAnimationFrame(() => {
      this.#rafHandle = undefined;
      this.run();
    });
  }

  stop(): void {
    if (this.#rafHandle) {
      cancelAnimationFrame(this.#rafHandle);
    }
  }
}
