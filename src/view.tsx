import { useEffect, useRef, useState } from "preact/hooks";

import { Viewport, Vec2, v2 } from "./math";
import { World } from "./world";
import { draw } from "./draw";
import { RenderLoop } from "./render";
import { DrawContext } from "./draw_context";

export type WorldViewProps = {
  world: World,
  viewport: Viewport,
  size: Vec2,
  onViewportChange?: (viewport: Viewport) => void;
  drawNodes?: boolean;
  drawLinks?: boolean;
  drawShapes?: boolean;
  debug?: boolean;
};

type PanState = { viewportStart: Vec2, mouseStart: Vec2 };

export const WorldView = ({ world, viewport, size, onViewportChange, drawNodes, drawLinks, drawShapes, debug }: WorldViewProps) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const viewportRef = useRef(viewport);
  const drawOptsRef = useRef({ drawNodes, drawLinks, drawShapes, debug });
  const panStateRef = useRef<PanState | undefined>();

  const [ctx, setCtx] = useState<DrawContext | undefined>();

  const viewportChange = (viewport: Viewport): void => {
    if (onViewportChange) {
      onViewportChange(viewport);
    }
  };

  useEffect(() => {
    const canvas = canvasRef.current;

    if (!canvas) {
      return;
    }

    setCtx(DrawContext.fromCanvas(canvas));
  }, [canvasRef.current]);

  useEffect(() => {
    viewportRef.current = viewport;
  }, [viewport]);

  useEffect(() => {
    drawOptsRef.current = {
      drawNodes,
      drawLinks,
      drawShapes,
      debug,
    };
  }, [drawNodes, drawLinks, drawShapes, debug]);

  useEffect(() => {
    if (!ctx) {
      return;
    }

    const loop = new RenderLoop(() => {
      draw(world, ctx, viewportRef.current, drawOptsRef.current);
    });

    loop.run();

    return () => {
      loop.stop();
    };
  }, [ctx]);

  const onMouseDown = (evt: MouseEvent): void => {
    if (!ctx) {
      return;
    }

    const mousePos = v2(evt.clientX, evt.clientY).sub(ctx.position());

    panStateRef.current = {
      mouseStart: mousePos,
      viewportStart: viewportRef.current.center,
    };
  };

  const onMouseMove = (evt: MouseEvent): void => {
    if (!ctx) {
      return;
    }

    const mousePos = v2(evt.clientX, evt.clientY).sub(ctx.position());

    const panState = panStateRef.current;
    const viewport = viewportRef.current;

    if (panState) {
      const mouseDelta = mousePos.sub(panState.mouseStart);
      const scale = viewport.scale();
      const newViewport = new Viewport(
        panState.viewportStart.sub(mouseDelta.scale(1 / scale)),
        viewport.zoom,
      );
      panStateRef.current = {
        viewportStart: newViewport.center,
        mouseStart: mousePos,
      };
      viewportChange(newViewport);
    }
  };

  const onMouseUp = (): void => {
    panStateRef.current = undefined;
  };

  const onMouseLeave = (): void => {
    panStateRef.current = undefined;
  };

  const onWheel = (evt: WheelEvent): void => {
    evt.preventDefault();

    const newViewport = new Viewport(
      viewportRef.current.center,
      viewportRef.current.zoom - evt.deltaY / 1000,
    );

    viewportChange(newViewport);
  };

  return <canvas
    ref={canvasRef}
    width={size.x}
    height={size.y}
    onMouseDown={onMouseDown}
    onMouseMove={onMouseMove}
    onMouseUp={onMouseUp}
    onMouseLeave={onMouseLeave}
    onWheel={onWheel}
    className="ui"
  />;
};
