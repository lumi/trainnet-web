import { ServerFrame } from "./connection";

class Reader {
  #idx: number;
  #data: DataView;

  constructor(data: DataView) {
    this.#data = data;
    this.#idx = 0;
  }

  readU8(): number {
    const num = this.#data.getUint8(this.#idx);
    this.#idx += 1;
    return num;
  }

  readU16(): number {
    const num = this.#data.getUint16(this.#idx, true);
    this.#idx += 2;
    return num;
  }

  readU32(): number {
    const num = this.#data.getUint32(this.#idx, true);
    this.#idx += 4;
    return num;
  }

  readI8(): number {
    const num = this.#data.getInt8(this.#idx);
    this.#idx += 1;
    return num;
  }

  readI16(): number {
    const num = this.#data.getInt16(this.#idx, true);
    this.#idx += 2;
    return num;
  }

  readI32(): number {
    const num = this.#data.getInt32(this.#idx, true);
    this.#idx += 4;
    return num;
  }

  readF32(): number {
    const num = this.readI32();
    return num / 1000;
  }
}

export function readFrame(data: DataView): ServerFrame {
  const reader = new Reader(data);

  const code = reader.readU8();

  switch (code) {
    case 1: {
      const time = reader.readF32();
      const trainCount = reader.readU16();
      const trains = [];
      for (let i = 0; i < trainCount; i++) {
        const id = reader.readU32();
        const x = reader.readF32();
        const y = reader.readF32();
        const rot = reader.readF32();
        const speed = reader.readF32();
        const path_length = reader.readU16();
        const wagonCount = reader.readU8();
        const wagons = [];
        for (let i = 0; i < wagonCount; i++) {
          const x = reader.readF32();
          const y = reader.readF32();
          const rot = reader.readF32();
          const length = reader.readF32();
          wagons.push({
            pos: [x, y] as [number, number],
            dir: [Math.cos(rot), Math.sin(rot)] as [number, number],
            length
          });
        }
        trains.push({
          id,
          pos: [x, y] as [number, number],
          dir: [Math.cos(rot), Math.sin(rot)] as [number, number],
          speed,
          path_length,
          wagons,
        });
      }
      return { type: "dynamic", time, trains };
    }

    case 2: {
      const delNodeCount = reader.readU32();
      const delLinkCount = reader.readU32();
      const delShapeCount = reader.readU32();
      const addNodeCount = reader.readU32();
      const addLinkCount = reader.readU32();
      const addShapeCount = reader.readU32();

      const del_nodes: number[] = [];
      const del_links: [number, number][] = [];
      const del_shapes: number[] = [];

      const add_nodes: [number, [number, number]][] = [];
      const add_links: [[number, number], number][] = [];
      const add_shapes: [number, number, [number, number][]][] = [];

      for (let i = 0; i < delNodeCount; i++) {
        const id = reader.readU32();
        del_nodes.push(id);
      }

      for (let i = 0; i < delLinkCount; i++) {
        const start = reader.readU32();
        const end = reader.readU32();
        del_links.push([start, end]);
      }

      for (let i = 0; i < delShapeCount; i++) {
        const id = reader.readU32();
        del_shapes.push(id);
      }

      for (let i = 0; i < addNodeCount; i++) {
        const id = reader.readU32();
        const x = reader.readF32();
        const y = reader.readF32();
        add_nodes.push([id, [x, y]]);
      }

      for (let i = 0; i < addLinkCount; i++) {
        const start = reader.readU32();
        const end = reader.readU32();
        const block_id = reader.readU32();
        add_links.push([[start, end], block_id]);
      }

      for (let i = 0; i < addShapeCount; i++) {
        const id = reader.readU32();
        const type = reader.readU8();
        const count = reader.readU32();
        const points: [number, number][] = [];
        for (let i = 0; i < count; i++) {
          const x = reader.readF32();
          const y = reader.readF32();
          points.push([x, y]);
        }
        add_shapes.push([id, type, points]);
      }

      return {
        type: "bulk",
        del_nodes,
        del_links,
        del_shapes,
        add_nodes,
        add_links,
        add_shapes,
      };
    }

    default:
      throw new Error(`invalid frame code ${code}`);
  }
}
