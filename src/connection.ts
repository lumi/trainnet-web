import { EventEmitter } from "@foxify/events";
import { Aabb2 } from "./math";
import { readFrame } from "./protocol";

export type ClientFrame = {
  type: "set_viewport", min_x: number, max_x: number, min_y: number, max_y: number
};

export type ServerFrame =
  | {
    type: "dynamic",
    time: number,
    trains: {
      id: number,
      pos: [number, number],
      dir: [number, number],
      speed: number,
      path_length: number,
      wagons: {
        pos: [number, number],
        dir: [number, number],
        length: number
      }[]
    }[],
  }
  | {
    type: "bulk",
    del_nodes: number[],
    del_links: [number, number][],
    del_shapes: number[],
    add_nodes: [number, [number, number]][],
    add_links: [[number, number], number][]
    add_shapes: [number, number, [number, number][]][],
  }

export type ConnectionEvents = {
  connect: () => void,
  disconnect: () => void,
  frame: (frame: ServerFrame) => void,
};

export type ConnectionStatus = "init" | "connected" | "disconnected";

export class Connection extends EventEmitter<ConnectionEvents> {
  #ws: WebSocket;

  #status: ConnectionStatus;

  #sentViewport: Aabb2 | undefined;

  constructor(url: string) {
    super();

    this.#ws = new WebSocket(url);
    this.#ws.binaryType = "arraybuffer";
    this.#sentViewport = undefined;
    this.#status = "init";

    this.#installHandlers();
  }

  #installHandlers(): void {
    this.#ws.onopen = () => {
      this.emit("connect");
      this.#status = "connected";
      if (this.#sentViewport) {
        this.send({ type: "set_viewport", ...this.#sentViewport });
      }
    };

    this.#ws.onclose = () => {
      this.emit("disconnect");
      this.#status = "disconnected";
    };

    this.#ws.onerror = (_err: Event) => {
      this.#status = "disconnected";
    };

    this.#ws.onmessage = (evt) => {
      const data = evt.data;
      if (data instanceof ArrayBuffer) {
        const frame: ServerFrame = readFrame(new DataView(data));
        this.emit("frame", frame);
      }
    }
  }

  setViewport(viewport: Aabb2): void {
    this.#sentViewport = viewport;
    if (this.#status === "connected") {
      this.send({ type: "set_viewport", ...viewport });
    }
  }

  send(frame: ClientFrame): void {
    console.log("sending", frame);
    this.#ws.send(JSON.stringify(frame));
  }

  close(): void {
    this.#ws.close();
  }
}


