export function formatTimestamp(ts: number): string {
  const milliseconds = Math.floor((ts % 1) * 1000);
  const seconds = Math.floor(ts % 60);
  const minutes = Math.floor((ts % 3600) / 60);
  const hours = Math.floor((ts % 86400) / 3600);
  const days = Math.floor((ts % 86400) / 3600);

  return `${days} ${String(hours).padStart(2, "0")}:${String(minutes).padStart(2, "0")}:${String(seconds).padStart(2, "0")}.${String(milliseconds).padStart(3, "0")}`;
}
