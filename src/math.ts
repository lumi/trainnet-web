export class Vec2 {
  readonly x: number;
  readonly y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
    Object.freeze(this);
  }

  static fromArray(nums: number[]): Vec2 {
    return v2(nums[0], nums[1]);
  }

  static lerp(t: number, a: Vec2, b: Vec2): Vec2 {
    return new Vec2(
      (1 - t) * a.x + t * b.x,
      (1 - t) * a.y + t * b.y);
  }

  static distance(a: Vec2, b: Vec2): number {
    return b.sub(a).magnitude();
  }

  angle(): number {
    return Math.atan2(this.y, this.x);
  }

  add(other: Vec2): Vec2 {
    return new Vec2(this.x + other.x, this.y + other.y);
  }

  sub(other: Vec2): Vec2 {
    return new Vec2(this.x - other.x, this.y - other.y);
  }

  scale(factor: number): Vec2 {
    return new Vec2(this.x * factor, this.y * factor);
  }

  magnitude(): number {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  toString(): string {
    return `(${Math.floor(this.x)}, ${Math.floor(this.y)})`;
  }
}

export function v2(x: number, y: number): Vec2 { return new Vec2(x, y); }

export class Aabb2 {
  readonly min_x: number;
  readonly max_x: number;
  readonly min_y: number;
  readonly max_y: number;

  constructor(min: Vec2, max: Vec2) {
    this.min_x = min.x;
    this.min_y = min.y;
    this.max_x = max.x;
    this.max_y = max.y;
    Object.freeze(this);
  }

  containsPoint(pos: Vec2): boolean {
    return (
      pos.x >= this.min_x
      && pos.x <= this.max_x
      && pos.y >= this.min_y
      && pos.y <= this.max_y
    );
  }

  min(): Vec2 {
    return v2(this.min_x, this.min_y);
  }

  max(): Vec2 {
    return v2(this.max_x, this.max_y);
  }

  size(): Vec2 {
    return v2(this.max_x - this.min_x, this.max_y - this.min_y);
  }

  center(): Vec2 {
    return v2((this.max_x + this.min_x) / 2, (this.max_y + this.min_y) / 2);
  }

  scale(factor: number): Aabb2 {
    const hsize = this.size().scale(0.5);
    const center = this.center();
    const v = hsize.scale(factor);
    return new Aabb2(center.sub(v), center.add(v));
  }
}

export function aabb2(min: Vec2, max: Vec2): Aabb2 { return new Aabb2(min, max); }

export class Viewport {
  readonly center: Vec2;
  readonly zoom: number;

  constructor(center: Vec2, zoom: number) {
    this.center = center;
    this.zoom = zoom;
    Object.freeze(this);
  }

  scale(): number {
    return 10 * Math.pow(2, this.zoom);
  }

  toAabb(canvasSize: Vec2): Aabb2 {
    const iscale = 1 / this.scale();
    const hsize = canvasSize.scale(0.5);
    return aabb2(
      this.center.sub(hsize.scale(iscale)),
      this.center.add(hsize.scale(iscale)));
  }
}

export function angleInterpolate(t: number, rota: number, rotb: number): number {
  const drot = (rotb - rota) % (Math.PI * 2);
  const angle = 2 * drot % (Math.PI * 2) - drot;
  return rota + angle * t;
}
