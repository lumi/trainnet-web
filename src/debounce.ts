export function debouncer(delayMs: number, repeat?: boolean): (cb: () => void) => void {
  let timeout: number | undefined = undefined;

  return (cb) => {
    if (!repeat && timeout) {
      clearTimeout(timeout);
      timeout = undefined;
    }

    if (!repeat || !timeout) {
      timeout = setTimeout(() => {
        timeout = undefined;
        cb();
      }, delayMs);
    }
  };
}
